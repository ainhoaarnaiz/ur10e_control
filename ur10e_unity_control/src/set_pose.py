#!/usr/bin/env python3

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
from geometry_msgs.msg import Pose, PoseStamped, Transform
import math
from sensor_msgs.msg import JointState

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial',anonymous=True)
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group = moveit_commander.MoveGroupCommander("manipulator")

pose_target = PoseStamped()
pose_target.pose.position.x = 0.5733226672195488
pose_target.pose.position.y = 0.27526239013854864
pose_target.pose.position.z = 0.6770628851869593

pose_target.pose.orientation.x = 0.0017539950657710186
pose_target.pose.orientation.y = 0.7070659124220542
pose_target.pose.orientation.z = 0.706516391362398
pose_target.pose.orientation.w = 0.029821263023757268

group.set_pose_target(pose_target.pose)
group.go(wait=True)
group.stop()
group.clear_pose_targets()

 