#!/usr/bin/env python3

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
from geometry_msgs.msg import Pose, PoseStamped, Transform
import math
from sensor_msgs.msg import JointState
 
class pick_class():

  def __init__(self):
    self.robot = moveit_commander.RobotCommander()
    self.scene = moveit_commander.PlanningSceneInterface()
    self.group = moveit_commander.MoveGroupCommander("manipulator")

    self.sub = rospy.Subscriber("/ee_pick", PoseStamped, self.callback)

  def callback(self,data):
    self.group.set_max_velocity_scaling_factor(velocity_class.velocity_target.velocity[0])

    # approach to target
    self.pose_target = PoseStamped()
    self.pose_target.pose.position.x = data.pose.position.x
    self.pose_target.pose.position.y = data.pose.position.y
    self.pose_target.pose.position.z = data.pose.position.z

    self.pose_target.pose.orientation.x = data.pose.orientation.x
    self.pose_target.pose.orientation.y = data.pose.orientation.y
    self.pose_target.pose.orientation.z = data.pose.orientation.z
    self.pose_target.pose.orientation.w = data.pose.orientation.w

    self.group.set_pose_target(self.pose_target.pose)
    self.group.go(wait=True)
    self.group.stop()
    self.group.clear_pose_targets()



class place_class():

  def __init__(self):
    self.robot = moveit_commander.RobotCommander()
    self.scene = moveit_commander.PlanningSceneInterface()
    self.group = moveit_commander.MoveGroupCommander("manipulator")

    self.sub = rospy.Subscriber("/ee_place", PoseStamped, self.callback)

  def callback(self,data):
    self.group.set_max_velocity_scaling_factor(velocity_class.velocity_target.velocity[0])

    # approach to target
    self.pose_target = PoseStamped()
    self.pose_target.pose.position.x = data.pose.position.x
    self.pose_target.pose.position.y = data.pose.position.y
    self.pose_target.pose.position.z = data.pose.position.z

    self.pose_target.pose.orientation.x = data.pose.orientation.x
    self.pose_target.pose.orientation.y = data.pose.orientation.y
    self.pose_target.pose.orientation.z = data.pose.orientation.z
    self.pose_target.pose.orientation.w = data.pose.orientation.w

    self.group.set_pose_target(self.pose_target.pose)
    self.group.go(wait=True)
    self.group.stop()
    self.group.clear_pose_targets()

class velocity_class():

  def __init__(self):
    self.robot = moveit_commander.RobotCommander()
    self.scene = moveit_commander.PlanningSceneInterface()
    self.group = moveit_commander.MoveGroupCommander("manipulator")
    rospy.Subscriber("/vel", JointState, self.callback)
    velocity_class.velocity_target = JointState()

  def callback(self,data):
    velocity_class.velocity_target.velocity = data.velocity
    print(velocity_class.velocity_target.velocity[0])


def main(args):
    rospy.init_node('target_pos', anonymous=True)
    velocity_class()
    pick_class()
    place_class()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")

if __name__ == '__main__':
    moveit_commander.roscpp_initialize(sys.argv)
    main(sys.argv)